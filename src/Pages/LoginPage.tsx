﻿import * as React from "react";
import { Button } from "office-ui-fabric-react/lib/Button";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { DefaultButton } from "office-ui-fabric-react/lib/Button";
import { HarvestAPI } from "../HarvestAPI";

export interface LoginPageProps
{
    onLogin: (domain: string, username: string, password: string) => void;
}

export interface LoginPageState
{
    domain: string;
    username: string;
    password: string;
    errorMessage: string;
}

export class LoginPage extends React.Component<LoginPageProps, LoginPageState>
{
    constructor(props: LoginPageProps)
    {
        super(props);

        this.state = {
            domain: "",
            username: "",
            password: "",
            errorMessage: null
        }

        Office.initialize = (reason: Office.InitializationReason) =>
        {
            let settings = Office.context.roamingSettings;
            let domain = settings.get('harvest-domain');
            let username = settings.get('harvest-user');
            let password = settings.get('harvest-password');

            this.setState({
                domain: domain || "",
                username: username || "",
                password: password || ""
            });
        }
    }

    public componentDidMount()
    {

    }

    private onLoginClick()
    {
        this.setState({ errorMessage: '' });

        if (this.state.domain && this.state.username && this.state.password)
        {
            let settings = Office.context.roamingSettings;
            settings.set('harvest-domain', this.state.domain);
            settings.set('harvest-user', this.state.username);
            settings.set('harvest-password', this.state.password);
            settings.saveAsync((result: Office.AsyncResult<any>) => 
            {
                //let api = new HarvestAPI(this.state.domain, this.state.username, this.state.password);
                let api = new HarvestAPI(this.state.domain, this.state.username);
                api.GetWhoAmI().then((data) =>
                {
                    console.log(data);

                    this.props.onLogin(
                        this.state.domain,
                        this.state.username,
                        this.state.password
                    );

                }).catch((error) =>
                {
                    console.error(error);
                    this.setState({ errorMessage: 'Error logging in: incorrect username, password or domain' });
                });
            });
        }
        else
        {
            this.setState({ errorMessage: 'Please fill in all fields' });
        }
    }

    public render()
    {
        return (
            <div className="content-container">
                <div id="content-header">
                    <div className="padding">
                        <p className="ms-font-xl ms-fontColor-themeDarkAlt ms-fontWeight-semilight">Log In to Harvest</p>
                    </div>
                </div>
                <div className="content">

                    <TextField
                        label="Subdomain"
                        value={this.state.domain}
                        onChanged={(value: string) => this.setState({ domain: value })}
                        placeholder="e.g. blueshiftco"
                    />

                    <TextField
                        label="Username"
                        value={this.state.username}
                        onChanged={(value: string) => this.setState({ username: value })}
                        placeholder="user@domain.com"
                    />

                    <TextField
                        label="Password"
                        value={this.state.password}
                        onChanged={(value: string) => this.setState({ password: value })}
                        placeholder="password"
                        type="password"
                    />

                    <DefaultButton onClick={this.onLoginClick.bind(this)}>Log In</DefaultButton>

                    <div className="error-message">{this.state.errorMessage}</div>
                </div>
            </div>
        );
    }
}
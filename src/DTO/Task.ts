﻿
export class Task
{
    id: number;
    name: string;
    billable: boolean;

    constructor()
    {
        this.id = -1;
        this.name = '';
        this.billable = false;
    }

    public static FromJson(taskJson: any)
    {
        let task = new Task();
        task.id = taskJson.task.id;
        task.name = taskJson.task.name;
        task.billable = taskJson.billable;
        
        //console.log(task);//**check if each task appears with its props(id, name, billable)//**
        return task;
    }
}
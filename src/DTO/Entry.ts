﻿
export class Entry
{
    id: number;
    spent_date: string;
    hours: number;
    notes: string;
    project_id: number;
    project_name: string;
    task_id: number;
    task_name: string;
    
    external_reference: {
        id: string,
        permalink: string
    };
    //external_ref_id: string;
    //external_ref_permalink: string;
    
    

    constructor()
    {

    }

    public static FromJson(eventJson: any): Entry
    {
        let entry = new Entry();
        entry.id = eventJson.id;
        entry.spent_date = eventJson.spent_date;
        entry.hours = eventJson.hours;
        entry.notes = eventJson.notes;
        entry.project_id = eventJson.project.id;
        entry.project_name = eventJson.project.name;
        entry.task_id = eventJson.task.id;
        entry.task_name = eventJson.task.name;
        
        if (eventJson.external_reference !==null) {
            entry.external_reference = {//**define first entry.external_reference.id and permalink
                id: eventJson.external_reference.id,
                permalink: eventJson.external_reference.permalink   
            }
        }
        
        
        //console.log('ENTRY' + JSON.stringify(entry));

        return entry;
    }
}
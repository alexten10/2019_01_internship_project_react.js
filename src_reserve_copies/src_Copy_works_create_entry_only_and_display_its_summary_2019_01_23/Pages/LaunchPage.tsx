﻿import * as React from "react";
import { Label } from "office-ui-fabric-react/lib/Label";
import { Spinner, SpinnerType } from "office-ui-fabric-react/lib/Spinner";

export interface LaunchPageProps
{
    
}

export interface LaunchPageState
{

}

export class LaunchPage extends React.Component<LaunchPageProps, LaunchPageState>
{
    constructor(props: LaunchPageProps)
    {
        super(props);
    }

    public componentDidMount()
    {

    }

    public render()
    {
        return (
            <div className="content-container">
                <Spinner type={ SpinnerType.large } label="Logging in..." />
            </div>
        );
    }
}
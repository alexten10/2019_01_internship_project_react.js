﻿
export class Entry
{
    //id: number;
    spent_date: string;
    //date: Date;
    hours: number;
    notes: string;

    //client: string;
    //project: string;
    project_id: number;
    //task: string;
    task_id: number;

    constructor()
    {

    }

    public static FromJson(eventJson: any): Entry
    {
        let entry = new Entry();
        //entry.id = eventJson.id;
        entry.spent_date = eventJson.spent_at;
        //entry.date = new Date(eventJson.spent_at);
        entry.hours = eventJson.hours;
        entry.notes = eventJson.notes;

        //entry.client = eventJson.client;
        //entry.project = eventJson.project;
        entry.project_id = eventJson.project_id;
        //entry.task = eventJson.task;
        entry.task_id = eventJson.task_id;
        console.log('ENTRY' + entry);

        return entry;
    }
}
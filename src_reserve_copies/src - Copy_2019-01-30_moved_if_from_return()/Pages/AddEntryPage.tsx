import * as React from "react";
import { Button } from "office-ui-fabric-react/lib/Button";
import { Dropdown, IDropdownOption } from "office-ui-fabric-react/lib/Dropdown";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { DefaultButton, PrimaryButton } from "office-ui-fabric-react/lib/Button";
import { Checkbox } from "office-ui-fabric-react/lib/Checkbox";
import { HarvestAPI } from "../HarvestAPI";
import { Entry } from "../DTO/Entry";

//to get rid of following warning in console
//WARNING: The icon "checkmark" was used but not registered. See http://aka.ms/fabric-icon-usage for more information.
import { initializeIcons } from '@uifabric/icons';
initializeIcons();



export interface AddEntryPageProps 
{
    api: HarvestAPI;
}



export interface AddEntryPageState 
{
    isComposeMode: boolean;
    
    projectList: DropdownOption[];
    taskList: DropdownOption[];
    projectId: number;
    taskId: number;
    hours: string;
    selectedNotes: string; //** typed notes in add-in's 'Notes' textarea
    outlookNotes: string; //** notes from Outlook value (Office.body)
    includeMeetingContent: boolean; //** checkbox status
    isSaving: boolean;
    
    errorMessage: string;
    successMsgOne: string;//** used when new entry successfully created or updated
    successMsgTwo: string;//** used if an entry updated second time
    errorMessageProject: string;//** used when no project is selected (dropdown 'Project')
    errorMessageTask: string;//** used when no task is selected (dropdown 'Task')
    
    entryId: number; //** harvest entry ID
    entryProjectName: string;
    entryTaskName: string;
    entryNotes: string;
    entryHours: string;
    entryDate: string;
    isEntryData: boolean;//if specific entry data is recieved
    
    entryProjectId: number;
    entryTaskId: number;
    
    entryExternalReferenceId: string;
    
    outlookId: string;
}



class DropdownOption implements IDropdownOption
{
    public key: number;
    public text: string;
    public subtitle: string;
}



interface ItemInfo 
{
    start: Date;
    end: Date;
    itemId: string;
    body: string;
}



export class AddEntryPage extends React.Component<AddEntryPageProps, AddEntryPageState>
{
    private projects: any;

    constructor(props: AddEntryPageProps) 
    {
        super(props);

        this.state = {
            isComposeMode: true, //** if run add-in in compose mode, dont allow to do anything, as this add-in doesnt work in that mode
            
            projectList: [],
            taskList: [],
            projectId: -1,
            taskId: -1,
            selectedNotes: "",
            outlookNotes: "",
            hours: "",
            includeMeetingContent: false,
            isSaving: false,
            
            errorMessage: null,
            successMsgOne: null,
            successMsgTwo: null, 
            errorMessageProject: null,
            errorMessageTask: null,
            
            entryId: null,
            entryProjectName: null,
            entryTaskName: null,
            entryNotes: null,
            entryHours: null,
            entryDate: null,
            isEntryData: false,
            
            entryProjectId: null,
            entryTaskId: null,
            
            entryExternalReferenceId: null,
            
            outlookId: null,
        }
    }



    /*
    **Get info from opened outlook task
    **
    **Results:
    **    this.state.outlookNotes = outlook note text
    **    this.state.hours = hours spent for the outlook task
    **    this.state.outlookId = ID of the outlook task to create an external reference
    **
    **Return: array
    **        (
    **            outlook task start time,
    **            outlook task end time,
    **            outlook task ID,
    **            outlook task note's text
    **        )
    **
    **If no success returns error message
    */
    private getItemInfo()
    {
        return new Promise<ItemInfo>((resolve, reject) => 
        {
            let item = Office.context.mailbox.item;
            
            // Detect if we're in Read or Compose mode (Office.context.mailbox.item.itemId is available in read mode only)
            if(item.itemId) // We're in read mode (not in compose mode - the mode where we can save or edit the task)
            {
                this.setState({ isComposeMode: false });
                
                item.body.getAsync(Office.CoercionType.Text, {}, (body) =>//**get body of the outlook note in text format
                {
                    let body_value = body.value.trim();//**remove spaces at the beginning and finish of the outlook note
                    //**if Outlook notes is not empty, save it to the 'this.state.outlookNotes'
                    if(body_value !== '') {
                        this.setState({ outlookNotes: "\n" + body_value });
                    }
                    
                    /////get number of hours spent for the outlook task
                    let start = new Date(item.start).getTime();//** ex. 1548997200000
                    let end = new Date(item.end).getTime();//** ex. 1548999900000
                    let hours = (end - start) / (60 * 60 * 1000); // Get number of hours
                    
                    //**from Outlook: if time spent for task is checkbox "All day" (= 24 hours), save it as 8 hours (as full work day)
                    if (hours==24) {
                        this.setState({ hours: '8' });
                    }
                    else {//**convert a number of hours into a string, keeping only 2 decimals and save
                        this.setState({ hours: hours.toFixed(2) });
                    }//**END if-else
                    
                    this.setState({outlookId: item.itemId});
                    
                    resolve({
                        start: item.start,
                        end: item.end,
                        itemId: item.itemId,
                        body: body_value
                    });
                });
            }
            else // We're in compose mode so this.state.isComposeMode = true by default
            {
                reject('you must use compose mode!');
            }//END if(item.itemId) - else
            
        });//END new Promise<ItemInfo>(resolve.reject)
    }//END private getItemInfo()



    /*
    **Get all entries from Harvest for a specific day (day of the opened outlook task)
    **Check (match) outlook task id against all tasks for the day from harvest
    **If match, that means the outlook task has been saved to harvest before
    **
    **Return: if match found, return matched harvest entry; if no match -return null
    */
    private getEntry(startDate: Date, itemId: string)
    {
        return this.props.api.GetEntriesForDay(startDate).then((entries) => 
        {
            //console.log("DATA BEFORE FILTER: " + entries.length);//**to check the number of entries we get as response
            //console.log("DATA BEFORE FILTER: " + JSON.stringify(entries));//* to check what data is gotten
            
            //** get the Harvest entry that matches outlook task id
            let retval = entries.filter((e) => 
            {
                //** not all existing Harvest entries have external_reference
                if(e.external_reference){
                    return e.external_reference.id === itemId;//** match every entry's external_reference.id against outlook task's id
                } else {
                    return false
                }
            });

            if(retval.length > 0) { //** if matching outlook ID is found
                return retval[0];//** return a matched_entry
            } else {
                return null;
            }
        })
        .catch((error) => {//**if no data of entries are recieved after the request
            console.error('Error retrieving entries data: ' + error);
        });
    }



    public componentDidMount() 
    {
        Promise.all([
            this.props.api.GetAvailProjects(),//get list of all projects from harvest
            this.getItemInfo(),//Get info from opened outlook task
        ]).then((results) => 
        {
            this.projects = results[0].projects;// 'this.projects' (list of all projects) is used by onProjectChange()
            let itemInfo = results[1];

            //** get list of projects for dropdown "Project"
            let list = this.projects.map((project) =>//**loop through the received data (list of projects) and define values for each project
            {
                return {
                    key: project.id,
                    text: project.name,
                    subtitle: 'Client: ' + project.client
                };
            });
            this.setState({ projectList: list });//** this is used to create dropdown options "Project"


            this.getEntry(itemInfo.start, itemInfo.itemId).then((matched_entry) => 
            {
                if(matched_entry)
                {
                    //** get all tasks that belong to the matched entry project_id to make the list
                    //** of tasks in the dropdown 'Task' with selected option as entry's task_id
                    let project = this.projects.filter((p) =>
                    {
                        return p.id === matched_entry.project_id;
                    });
                    //console.log("PEOJECT_VALUE: " + project.title);

                    //get list of tasks that belong to the entry's project
                    if (project && project.length > 0)
                    {
                        project = project[0];
                        let tasks = project.tasks.map((t) => {
                            return {
                                key: t.id,
                                text: t.name,
                                subtitle: t.billable ? 'Type: Billable ' : 'Type: Non-Billable '
                            };
                        }).sort((a, b) => {//**sort the results by subtitle first(billable or non), then by task name in alphabet order ??
                            var o1 = a.subtitle.toLowerCase();
                            var o2 = b.subtitle.toLowerCase();
                            var p1 = a.text.toLowerCase();
                            var p2 = b.text.toLowerCase();

                            if (o1 < o2) return -1;
                            if (o1 > o2) return 1;
                            if (p1 < p2) return -1;
                            if (p1 > p2) return 1;
                            return 0;
                        });
                        this.setState({taskList: tasks});
                    }//END if

                    //console.log("FINAL: " + JSON.stringify(matched_entry));//**check what data is incomming
                    this.setState({isEntryData: true}); //** if harvest entry is already exist, set a flag to display Edit form

                    this.setState({
                            entryId: matched_entry.id,
                            entryProjectName: matched_entry.project_name,
                            entryTaskName: matched_entry.task_name,
                            entryHours: matched_entry.hours,//** to display it in summary
                            hours: matched_entry.hours,//**to display in the edit form and is changeble to be saved(updated) with a new value
                            entryNotes: matched_entry.notes,//** to display it in the summary
                            selectedNotes: matched_entry.notes,//** to display it in the edit form for editing and saving on harvest
                            entryDate: matched_entry.spent_date,
                            entryProjectId: matched_entry.project_id,//** to use it as selected option in the dropdown 'Project'
                            projectId: matched_entry.project_id,
                            taskId: matched_entry.task_id,//** to use it as selected option in the dropdown 'Task', not changeble, used if user changes project and then selects it back - selected option will be initial (entry's saved task id)
                            entryTaskId: matched_entry.task_id,//** to use it as selected option in the dropdown 'Task', changeble,
                        });
                }
                else //if got null - didnt get any matched entry
                {
                    //console.log("FINAL: " + matched_entry);//**check if get null
                    
                    // User has not saved to harvest, let them pick a project and task
                }
            });
        });

    }//**END public componentDidMount()//**



    //**when the button "save" is clicked/**
    private onSaveClicked() 
    {
        let item: any = Office.context.mailbox.item;

        //dont allow to submit without selcting a project
        if (this.state.projectId == -1) {
            this.setState({errorMessageProject : "You must select a Project"});
            return;
        }

        //dont allow to submit without selcting a task
        if (this.state.taskId == -1) {
            this.setState({errorMessageTask : "You must select a Task"});
            return;
        }

        //** parse a string 'hours' and return a floating point number, then fix to 2 decimals with rounding (ex: 2.50)
        let hours: number = +parseFloat(this.state.hours).toFixed(2);
        
        //** if typed value of "Hours" is not a number after parsing, the value will be 0
        if (isNaN(hours)) {
            hours = 0;
        }

        //** now "hours" is a number, reset state.errorMessage and set true for state.isSaving
        this.setState({ isSaving: true, errorMessage: null });

        //console.log('DATE ' + startResult.value);//**ex.   Thu Jan 31 2019 00:00:00 GMT-0600 (Central Standard Time)
        //IMPORTANT!!!! Outlook uses Local Time Zone(-6 hours for Canada), while Harvest uses ?Greenwich
        //so if you create a new entry on Harvest and send to it the date from oulook, correct to ?Greenwich first
        //or send to Harvest only date(yyyy-mm-dd) without time
        //ex. if outlook task time is 2019-01-19 23:00 , harvest will save it on 2019-01-20 (next day)
        
        //**preparing date for the request to create a new entry
        let start_date = item.start;//ex.Thu Jan 31 2019 23:00:00 GMT-0600 (Central Standard Time)
        let year_of_new_entry = start_date.getFullYear(); //**get year 2019
        let month_of_new_entry = ("0" + (start_date.getMonth() + 1)).slice(-2);//get 2 digit month, ex. 01
        let day_of_new_entry = ("0" + start_date.getDate()).slice(-2);// get 2 digit day, ex 09
        
        let date_of_new_entry = year_of_new_entry + '-'
                               +month_of_new_entry + '-'
                               +day_of_new_entry;//**ex. 2019-01-31
        
        //**prepare data for the new entry, which will be sent to create a new Harvest entry
        let newEntry = new Entry();
        newEntry.spent_date = date_of_new_entry;
        newEntry.hours = hours;
        newEntry.project_id = this.state.projectId;
        newEntry.task_id = this.state.taskId;
        newEntry.external_reference = {
            id: item.itemId, //**outlook task's Id
            permalink: 'https://outlook.office.com/calendar/item/' + encodeURIComponent(item.itemId)
            //**must be encoded as the value has special characters
            //**use encodeURIComponent, NOT encodeURI. encodeURI doesnt encode  / ? : @ & = + $ #
        };
        
        let notes = this.state.selectedNotes;
        //** if checkbox "Include Outlook notes to be Saved on Harvest?" checked
        //** combine typed notes from 'Notes' textarea with Outlook notes
        if (this.state.includeMeetingContent)
        {
            notes += '\n\n Notes from outlook below:' + this.state.outlookNotes;
        }
        newEntry.notes = notes;
        console.log("NEW_ENTRY_DATA: " + JSON.stringify(newEntry));

        //**send data about new entry to the method CreateEntry() and if success save that data into this.state to display on success msg
        this.props.api.CreateEntry(newEntry).then((data: any) =>//** if just 'then(data) - typescript cant get data from array object, ex. data.id. so use 'then(data: any)'
        {
            console.log('Create_entry_RESPONSE: ' + JSON.stringify(data));
            this.setState({ isSaving: false, successMsgOne: "Entry is Created! Please, restart Add-In" });
            
            this.setState({ entryId: data.id,
                            entryProjectName: data.project.name,
                            entryTaskName: data.task.name,
                            entryNotes: data.notes,
                            entryHours: data.hours,
                            entryDate: data.spent_date,
                         });
            //console.log("ENTRY_ID" + JSON.stringify(this.state));
        }).catch((error) =>
        {
            console.error(error);
            this.setState({ isSaving: false, errorMessage: error });
        });//END catch()
                
    }//**END private onSaveClicked()



    //**when the button "edit" is clicked/**
    private onEditClicked() 
    {
        if (this.state.projectId == -1) {
            this.setState({errorMessageProject : "You must select a Project"});
            return;
        }

        if (this.state.taskId == -1) {
            this.setState({errorMessageTask : "You must select a Task"});
            this.setState({successMsgOne : null, successMsgTwo : null});
            return;
        }

        //** parse a string 'hours' and return a floating point number, then fix to 2 decimals with rounding
        let hours: number = +parseFloat(this.state.hours).toFixed(2);
        
        //** if typed value of "Hours" is not a number, the value will be 0
        if (isNaN(hours)) {
            hours = 0;
        }

        //**prepare data for the new entry
        let updatedEntry = new Entry();
        updatedEntry.project_id = this.state.projectId;
        updatedEntry.task_id = this.state.taskId;
        updatedEntry.spent_date = this.state.entryDate;
        updatedEntry.hours = hours;
        updatedEntry.notes = this.state.selectedNotes;
        //console.log(JSON.stringify(updatedEntry));
        
        this.props.api.UpdateEntry( this.state.entryId, updatedEntry).then((data: any) =>//** if just 'then(data) - typescript cant get data from array object, ex. data.id. so use 'then(data: any)'
        {
            //console.log('Create_entry_RESPONSE: ' + JSON.stringify(data));
            
            //** if user updated once the entry he gets "Entry is Updated!",
            //** if user updates in the same window the entry again he gets "Upadated!" ans so on
            if (this.state.successMsgOne == null) {
                this.setState({ successMsgOne: "Entry is Updated!", successMsgTwo: null });
            }
            else {
                this.setState({ successMsgOne: null, successMsgTwo: "Upadated!" });
            }
            
            this.setState({ entryId: data.id,
                            entryProjectName: data.project.name,
                            entryTaskName: data.task.name,
                            entryHours: data.hours,
                            hours: data.hours,
                            entryNotes: data.notes,
                            selectedNotes: data.notes,
                            entryDate: data.spent_date,
                            entryTaskId: data.task.id,
                            entryProjectId: data.project.id,
                            isEntryData: true
                         });
            //console.log("ENTRY_ID" + JSON.stringify(this.state));
        })

    }//**END private onEditClicked()



    public render()
    {
        let disableAllFields = this.state.isSaving;
        let disableTaskDropdown = this.state.projectId === -1;

        const mainDivStyling = {
            paddingLeft: '10px',
            paddingRight: '10px',
            paddingBottom: '50px'
        }

        //** red color font for displaying error messages
        const errorColor = {
            color: 'red',
            fontSize: '1.5em'
        };

        //** green color font for displaying success message when an entry is created
        const successColor = {
            color: 'green',
            fontSize: '1.5em'
        };

        const redColor = {
            color: 'red'
        };








        // let projectOptions = [ { key: '-1', text: 'Loading...' } ];

        // if(this.state.projects.length > 0)
        // {
        //     projectOptions = this.state.projects.map((p) => {
        //         return {
        //             key: p.id,
        //             text: p.name
        //         };
        //     })

        //     if(this.state.formState === FormState.New)
        //     {
        //         projectOptions.unshift({ key: '-1', text: 'Select a Project' });
        //     }
        // }









        let success_edit_msg = null;
        let entry_form = null;
        let create_form_or_summary_of_created = null;
        let content = null;

        //** display different success messages each time when an entry is edited
        if (this.state.successMsgOne !== null)
        {
            success_edit_msg = (
                <div>
                    <span style={successColor}>{this.state.successMsgOne}</span>
                </div>
            );
        }
        else
        {
            success_edit_msg = (
                <div>
                    <span style={successColor}>{this.state.successMsgTwo}</span>
                </div>
            );
        }

        //** display create new entry form,
        //** if a new entry created, show new entry's summary and dont show create form
        if (this.state.successMsgOne == null && this.state.successMsgTwo == null)
        {
            create_form_or_summary_of_created = (
                <div id='form'>
                    <h3>Create a new Harvest entry using this MS Calendar task</h3>

                    <Dropdown
                        label="Project"
                        placeholder="select a project"
                        options={this.state.projectList}
                        onChange={this.onProjectChanged.bind(this)}
                        errorMessage={this.state.errorMessageProject}
                        onRenderOption={this.onRenderOption.bind(this)}
                        disabled={disableAllFields}
                    />

                    <br />
                    <Dropdown
                        label="Task"
                        placeholder="select a task"
                        options={this.state.taskList}
                        onChange={this.onTaskChanged.bind(this)}
                        errorMessage={this.state.errorMessageTask}
                        onRenderOption={this.onRenderOption.bind(this)}
                        disabled={disableAllFields || disableTaskDropdown}
                    />

                    <br />
                    <TextField
                        label="Hours"
                        value={this.state.hours.toString()}
                        onChange={(e, value: string) => this.setState({ hours: value })}
                        onGetErrorMessage={this.onHoursErrorMessage.bind(this)}
                        disabled={disableAllFields}
                    />

                    <br />
                    <TextField
                        label="Notes"
                        value={this.state.selectedNotes}
                        onChange={(e, value: string) => this.setState({ selectedNotes: value })}
                        placeholder="Put your notes here (optional)"
                        multiline
                        autoAdjustHeight
                        disabled={disableAllFields}
                    />

                    <br />
                    <div><strong>Outlook notes: </strong><br />{this.state.outlookNotes}</div>

                    <br />
                    <Checkbox
                        label="Include Outlook notes to be Saved on Harvest?"
                        checked={this.state.includeMeetingContent}
                        onChange={(ev, checked: boolean) => { 
                            if (this.state.includeMeetingContent == false) { //** if checked, set true
                                this.setState({ includeMeetingContent: true }); 
                            }
                            else { //** if unchecked, set false
                                this.setState({ includeMeetingContent: false });
                            }
                        } }
                        disabled={disableAllFields}
                    />

                    <span className="error-message" style={errorColor} >{this.state.errorMessage}</span>

                    <br />
                    <PrimaryButton
                        onClick={this.onSaveClicked.bind(this)}
                        disabled={disableAllFields}>
                        Create
                    </PrimaryButton>

                </div>
            );
        }
        else{
            create_form_or_summary_of_created = (
                <div id="new_entry_created_summary">{/* if a new entry created, display new entry summary*/}
                    <br />
                    <h2><strong>Entry summary:</strong></h2><br />
                    <strong>Date:</strong> {this.state.entryDate}<br />
                    <strong>Project:</strong> {this.state.entryProjectName}<br />
                    <strong>Task:</strong> {this.state.entryTaskName}<br />
                    <strong>Hours:</strong> {this.state.entryHours}<br />
                    <strong>Notes:</strong> {this.state.entryNotes}
                </div>
            );
        }

        //** if the entry (that is based on the opened outlook task) already exists on harvest,
        //** display edit form (to edit thiat entry),
        //** if the entry doesnt exist, open create form (to create a new entry, based on outlook task)
        if( this.state.isEntryData == true )
        {
            entry_form = (
                <div id="edit_form"> 
                    <p style={redColor}>This outlook task has been already synchronized with Harvest entry.</p>

                    <h2>Change this entry info:</h2>

                    <Dropdown
                        label="Project"
                        placeholder="select a project"
                        options={this.state.projectList}
                        onChange={this.onProjectChanged.bind(this)}
                        selectedKey={this.state.projectId}
                        errorMessage={this.state.errorMessageProject}
                        onRenderOption={this.onRenderOption.bind(this)}
                        disabled={disableAllFields}
                    />

                    <br />
                    <Dropdown
                        label="Task"
                        placeholder="select a task"
                        options={this.state.taskList}
                        onChange={this.onTaskChanged.bind(this)}
                        selectedKey={this.state.taskId}
                        errorMessage={this.state.errorMessageTask}
                        onRenderOption={this.onRenderOption.bind(this)}
                        disabled={disableAllFields || disableTaskDropdown}
                    />

                    <br />
                    <TextField
                        label="Hours"
                        value={this.state.hours.toString()}
                        onChange={(e, value: string) => this.setState({ hours: value })}
                        onGetErrorMessage={this.onHoursErrorMessage.bind(this)}
                        disabled={disableAllFields}
                    />

                    <br />
                    <TextField
                        label="Notes"
                        value={this.state.selectedNotes}
                        onChange={(e, value: string) => this.setState({ selectedNotes: value })}
                        placeholder="Put your notes here (optional)"
                        multiline
                        autoAdjustHeight
                        disabled={disableAllFields}
                    />

                    {success_edit_msg}

                    {this.state.errorMessageTask ? (<div><p style={redColor}>You have an error. Please review the form.</p></div>) : ''}

                    <br />
                    <PrimaryButton
                        onClick={this.onEditClicked.bind(this)}
                        disabled={disableAllFields}>
                        Edit
                    </PrimaryButton>
                </div>
            );
        }
        else
        {
            entry_form =(
                <div id="create_entry_form">
                    {create_form_or_summary_of_created}
                </div>
            );
        }

        //** if add-in is opened in compose mode (edit OR save modes),
        //** display "Please, use read mode",
        //** otherwise display entry form (edit form OR create form)
        if(this.state.isComposeMode)
        {
            content =(
                <div>
                    <h1 style={redColor}> This add-in doesnt work in Outlook compose mode ( both Save or Edit task modes)</h1>
                    <h2>Please, use read mode</h2>
                </div>
            );
        }
        else
        {
            content = (
                <div>
                    {success_edit_msg}
                    {entry_form}
                </div>
            );
        }

        //** opening parenthesis MUST be right after 'return'! otherwise error.
        return (
            <div className="content-container" style={mainDivStyling}>
                <div className="content">
                    {content}
                </div>
            </div>
        );//END return()
        
    }//END render()



    //** get the list of tasks, when project is chosen from dropdown list "Project"
    //** uses project.id to get its tasks and put them into an array this.state.taskList 
    //** returns task.id - as key, task.name - as text, and task.billable - as subtitle 
    private onProjectChanged(e, value: DropdownOption)
    {
        this.setState({errorMessageProject: null});//** erase error message for 'Project' input

        //** when clicked on 'Project' dropdown reset task_id which is used as <option selected=... >, as its value is changing on every task click
        //** after reseting <option selected=...> will use entryTaskId which is fixed to the opened entry
        //** doing it is needed for "Change entry" form(not for "create entry" form). User has a chance always get back entry's initial preselected option
        this.setState({taskId: -1});

        //** loop through the array of projects
        let project = this.projects.filter((p) =>
        {
            return p.id === value.key;
        });
        //console.log("PEOJECT_VALUE: " + project.title);

        if (project && project.length > 0)
        {
            project = project[0];
            //console.log("selected project info: " + JSON.stringify(project));
            let tasks = project.tasks.map((t) => {
                return {
                    key: t.id,
                    text: t.name,
                    subtitle: t.billable ? 'Type: Billable ' : 'Type: Non-Billable '
                };
            }).sort((a, b) => {
                var o1 = a.subtitle.toLowerCase();
                var o2 = b.subtitle.toLowerCase();
                var p1 = a.text.toLowerCase();
                var p2 = b.text.toLowerCase();

                if (o1 < o2) return -1;
                if (o1 > o2) return 1;
                if (p1 < p2) return -1;
                if (p1 > p2) return 1;
                return 0;
            });

            this.setState({
                projectId: value.key,
                taskList: tasks
            });

            //1
            //in edit form mode, if user changes project from A to B and then returns back to A, selectedKey automatically selects the task
            //but the value of the taskId(used for saving changes) every time the project is changed is -1 and it gives error when press 'edit'(not allowed to submit -1)
            // so use entryTaskId (which is used as selectedKey dropdown option) to assign taskId value by default only if project matches that task
            //2
            //different projects can have the same tasks(names and ids), so asign taskId(to be used as selectedKey) only if user chooses the project that has been saved to harvest
            if (this.state.entryTaskId !== null) {

                if (project.id == this.state.entryProjectId) {
                    this.setState({taskId: this.state.entryTaskId});
                }
            }

        }//END if
    }//END onProjectChanged()



    //** when a task is chosen in the add-in, use task.id to set this.state.taskId
    private onTaskChanged(e, value: DropdownOption)
    {
        this.setState({
            taskId: value.key,
            errorMessageTask: null //**erase error message for 'Task' input
        });
    }



    //**check if entered value in HOURS field is a number
    private onHoursErrorMessage(value: string)
    {
        let num = +value;

        if (isNaN(num) === false)
        {
            return '';
        }
        else
        {
            return 'Value must be a number';
        }
    }



    //** fires function that creates <option> dropdown for Project and Task fields in the add-in 
    private onRenderOption(option: DropdownOption)
    {
        return (
            <div className="dropdown-twoline">
                <span className="ms-font-m title">{option.text}</span><br />
                <span className="ms-font-s subtitle">{option.subtitle}</span>
            </div>
        );
    }


}//END class AddEntryPage





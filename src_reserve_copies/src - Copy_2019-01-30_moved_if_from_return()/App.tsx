import * as React from "react";
import * as queryString from "querystring";
import { LoginPage } from "./Pages/LoginPage";
import { AddEntryPage } from "./Pages/AddEntryPage";
import { LaunchPage } from "./Pages/LaunchPage";
import { HarvestAPI } from "./HarvestAPI";

export interface AppProps
{
    
}

export interface AppState
{
    loaded: boolean;
    loggedIn: boolean;
    api: HarvestAPI;
    message: string;
}

export class App extends React.Component<AppProps, AppState>
{
    // Generate a new OAuth2 app in Harvest at https://id.getharvest.com/developers
    private static CLIENT_ID: string = 'YOp-cSXM0jX5KYlwaOYHI2Mk'; // OAuth2 App registered in Jon's Harvest


    constructor(props: AppProps)
    {
        super(props);

        this.state = {
            loaded: false,
            loggedIn: false,
            api: null,
            message: ''
        };
    }



    public componentDidMount()
    {
        Office.initialize = (reason: Office.InitializationReason) =>
        {
            this.ensureAuthenticated();
        }
    }



    private onLogin(domain: string, username: string, password: string)
    {
        this.setState({ loggedIn: true });
    }



    private ensureAuthenticated()
    {
        let settings = Office.context.roamingSettings;//**initialized when the task pane first opens
//**When receiving data from a web server, the data is always a string. JSON.parse(), and the data becomes a JavaScript object.
//**The slice() method returns the selected elements in an array, as a new array object.
//**The slice() method selects the elements starting at the given start argument(1), and ends at, but does not include, the given end argument(optional - not specified).
        let parsed = queryString.parse(window.location.search.length > 0 ? window.location.search.slice(1) : '');
        let token = '';
        let scope = '';

        // Detect query string parameters from auth
        if(parsed['access_token'])
        {
            token = parsed['access_token'];
            scope = parsed['scope'];
            
//**convert to a json string token and scope
//**messageParent - deliver msg from dialog box to its opener/parent page
            Office.context.ui.messageParent(JSON.stringify({ 
                token: token,
                scope: scope
            }));
        }
        // Otherwise use our saved token
        else
        {
            token = settings.get('harvest-token');
            scope = settings.get('harvest-scope');

            this.setState({ message: `Found token ${token} and scope ${scope}` });
        }

//**get authorized at Harvest and setState
//**passing to HarvestAPI token and scope(scope includes accountId value for authorization)
        let api = new HarvestAPI(token, scope);
        api.GetWhoAmI().then((data) => 
        {
            this.setState({ loaded: true, loggedIn: true, api: api });
        }).catch((error) => 
        {
            // We're not logged in, show Harvest login button
            this.setState({ loaded: true, loggedIn: false });
        });
    }




    public render()
    {
        let visiblePage: any;

        if (this.state.loaded)
        {
            if (this.state.loggedIn)
                visiblePage = <AddEntryPage api={this.state.api} />;
            else
            {
                visiblePage = (
                    <div>
                        Please,&nbsp;
                        <a href="javascript:void(0);" onClick={this.onHarvestLogin.bind(this)}>
                            log in&nbsp;
                        </a> 
                        to Harvest. And press 'Authorize App' button.
                        <h2>This add-in works with the <u>new Outlook version</u> only</h2>
                    </div>
                    
                );
            }
                //visiblePage = <LoginPage onLogin={this.onLogin.bind(this)} />;
        }
        else
        {
            visiblePage = <LaunchPage />;
        }

        return (
            <div>
                {/*<p>{this.state.message}</p>*/}
                {visiblePage}
            </div>
        );
    }



    private onHarvestLogin()
    {
        Office.context.ui.displayDialogAsync(
            `https://id.getharvest.com/oauth2/authorize?client_id=${App.CLIENT_ID}&response_type=token`, 
            { height: 50, width: 30 }, 
            (result) => 
            {
                let dialog =  result.value;
                dialog.addEventHandler(Office.EventType.DialogMessageReceived, (arg) => 
                {
                    var msg = JSON.parse(arg.message);

                    let settings = Office.context.roamingSettings;
                    settings.set('harvest-token', msg.token);
                    settings.set('harvest-scope', msg.scope);
                    settings.saveAsync(() => {
                        this.ensureAuthenticated();
                    });

                    dialog.close();
                });
            }
        );
    }
}
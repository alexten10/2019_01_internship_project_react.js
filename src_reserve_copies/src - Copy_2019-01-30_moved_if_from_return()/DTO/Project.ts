﻿import { Task } from "./Task";

export class Project
{
    id: number;
    name: string;
    client: string;
    client_id: number;
    tasks: Task[];

    constructor()
    {
        this.id = -1;
        this.name = '';
        this.client = '';
        this.client_id = -1;
        this.tasks = [];
    }

    public static FromJson(projectJson: any)
    {
        let project = new Project();
        
        project.id = projectJson.project.id;
        project.name = projectJson.project.name;
        project.client = projectJson.client.name;
        project.client_id = projectJson.client.id;

        if (projectJson.task_assignments)
        {
            projectJson.task_assignments.forEach((t) =>
            {
                project.tasks.push(Task.FromJson(t));
            });
        }

        //console.log(project);//**check if each project appears with its props
        return project;
    }
}


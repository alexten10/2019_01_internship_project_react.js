import "es6-promise/auto";
import "whatwg-fetch";

import { Entry } from "./DTO/Entry";
import { Project } from "./DTO/Project";

export class HarvestAPI
{
    private token: string = '';
    private scope: string = '';
    private accountId: string = '';
    private apiRoot: string = "https://api.harvestapp.com/v2";

    constructor(token: string, scope: string)
    {
        this.token = token;
        this.scope = decodeURIComponent(scope);
        this.accountId = this.scope.split(':')[1];
    }

    public GetWhoAmI(): Promise<any>
    {
        return this.getData(`/users/me`);
    }


    /*
    ########################################################################################################################
    ### getting ALL entries from harvest for a specific day#################
    
    public GetEntriesForDay(date: Date): Promise<any>
    {
        let promise = new Promise((resolve, reject) =>
        {
            let year = date.getFullYear();
            let month = ("0" + (date.getMonth() + 1)).slice(-2)//**add 0 if month is 1-9, ex: 01, 02
            let day = ("0" + date.getDate()).slice(-2);
            let date_for_entries = year + '-' + month + '-' + day;
            //console.log("CONVERTED: " + start_day);
            //console.log(date_start);
            //**get ALL entries for the date range (1 specific day)
            this.getData(`/time_entries?from=${date_for_entries}&to=${date_for_entries}`).then((response: any) => 
            {
                if (response)
                    {
                        console.log(response);
                        let entries = [];
                        response.time_entries.forEach((e) =>//**put all entries into an array
                        {
                            entries.push(Entry.FromJson(e));
                        });
                        let projects = [];
                        response.projects.forEach((p) =>
                        {
                            projects.push(Project.FromJson(p));
                        });
                        resolve({
                            entries: entries,
                            projects: projects
                        });
                    }
                    else
                    {
                        reject('Received invalid data from web service');
                    }
            });
        });
        return promise;
    }//**END public GetEntriesForDay()
############################################################################################################################
*/


    public GetAvailProjects(date: Date): Promise<any>
    {
        let promise = new Promise((resolve, reject) =>
        {
            //**get list of all projects and entries available on harvest for a specific day (outlook calendar day)
            this.getData(`/users/me/project_assignments`).then((response: any) => 
            {
                if (response)
                {
                    //console.log(response);//**see what data is in response 

                    //**put all returned from Harvest projects into an array
                    let projects = [];
                    response.project_assignments.forEach((pr) => {
                        projects.push(Project.FromJson(pr));
                    });
                    console.log(projects);
                    //console.log('number of projects ' + projects.length);//**check how many projects we got from Harvest

                    resolve({ //if success return projects array
                        projects: projects
                    });
                }
                else
                {
                    reject('Received invalid data from web service');
                }
            });
        });

        return promise;
    }//**END public GetAvailProjects()


    //public CreateEntry(date: Date, hours: number, projectId: number, taskId: number, notes: string)
    public CreateEntry(entry)
    {
        let promise = new Promise((resolve, reject) =>
        {
            this.sendData(`/time_entries`, entry).then((response: any) => 
            {
                if (response)
                    {
                        //console.log('Create_entry_RESPONSE: ' + JSON.stringify(response));//**see what data is in response 
                        resolve(response);
                    }
                    else
                    {
                        reject('Received invalid data from web service');
                    }//END if-else
            });//END sendData()
        });//END let promise

        return promise;
    }//END createEntry()


    //** get entry info by entry_id
    public GetEntry(entry_id: number)
    {
        let promise = new Promise((resolve, reject) =>
        {
            //**get list of all projects and entries available on harvest
            this.getData(`/time_entries/` + entry_id).then((response: any) => 
            {
                if (response)
                {
                    //console.log("ENTRY_ID_RESPONSE: " + JSON.stringify(response));//**see what data is in response 
                    resolve(response);//**if successfully got data by entry id, return response data
                }
                else
                {
                    reject('Received invalid data from web service');
                }
            });
        });

        return promise;
    }//**END public GetEntry()


    //update entry info by using entry_id
    public UpdateEntry(entry, entryId: any)
    {
        let promise = new Promise((resolve, reject) =>
        {
            this.updateData(`/time_entries/` + entryId, entry).then((response: any) => 
            {
                if (response)
                    {
                        //console.log('Create_entry_RESPONSE: ' + JSON.stringify(response));//**see what data is in response 
                        resolve(response);
                    }
                    else
                    {
                        reject('Received invalid data from web service');
                    }//END if-else
            });//END sendData()
        });//END let promise

        return promise;
    }


    //** send data to the harvest to update entry's info
    private updateData(endpointUrl: string, data_to_update: string)
    {
        let converted_data_to_send = JSON.stringify(data_to_update);
        //console.log('JSON? :' + converted_data_to_send);
        
        let options: RequestInit = {
            method: 'PATCH',
            cache: 'no-cache',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Agent': 'OutlookAddIn (jon@blueshiftco.com)',
                'Authorization': `Bearer ${this.token}`,
                'Harvest-Account-Id': `${this.accountId}`
            },
            body: converted_data_to_send//** convert data into JSON to send to harvest server
        };
        console.log(options);
        console.log(this.apiRoot + endpointUrl);
        
        return fetch(this.apiRoot + endpointUrl, options).then((response) =>
        {
            console.log('RESPOSE_FROM_SERVER: ' + response.status);
            if(response.status == 200)//**if success, response status=200 OK}
            {
                return response.json();//** return parsed into json response data
            }
            else
            {
                if(response.status === 401)
                    throw new Error('Unauthorized');
                else
                    throw new Error('Error getting data from service: ' + response.statusText);
            }//**END if-else
        });//**END return fetch()
    }//**END private updateData()


    //**get data depending on endpointUrl that is passed to this function with calling
    //**(ex: auth, get entries for a day, get all projects avilable for the day, get specific entry info)
    private getData(endpointUrl: string)
    {
        let options: RequestInit = {
            method: 'GET',
            cache: 'no-cache',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Agent': 'OutlookAddIn (jon@blueshiftco.com)',
                'Authorization': `Bearer ${this.token}`,
                'Harvest-Account-Id': `${this.accountId}`
            }
        };
        
        //**fetch() - make server request to Harvest, Fetch returns a Promise
        //**syntax: fetch(option1 - url of the API, option - headers)
        return fetch(this.apiRoot + endpointUrl, options).then((response) =>
        {
            if(response.ok)//**if success, response has {ok:true}
            {
                return response.json();//** transform response into json to handle data
            }
            else
            {
                if(response.status === 401)
                    throw new Error('Unauthorized');
                else
                    throw new Error('Error getting data from service: ' + response.statusText);
            }//**END if-else
        });//**END return fetch()
    }//**END private getData()


    //** send data to Harvest server using POST method to create an entry
    private sendData(endpointUrl: string, data_to_send: string)
    {
        let converted_data_to_send = JSON.stringify(data_to_send);
        //console.log('JSON? :' + converted_data_to_send);
        
        let options: RequestInit = {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'User-Agent': 'OutlookAddIn (jon@blueshiftco.com)',
                'Authorization': `Bearer ${this.token}`,
                'Harvest-Account-Id': `${this.accountId}`
            },
            body: converted_data_to_send//** convert data into JSON to send to harvest server
        };
        console.log(options);
        console.log(this.apiRoot + endpointUrl);
        
        return fetch(this.apiRoot + endpointUrl, options).then((response) =>
        {
            console.log('RESPOSE_FROM_SERVER: ' + response.status);
            if(response.status == 201)//**if success, response status=201 created}
            {
                return response.json();//** return parsed into json response data
            }
            else
            {
                if(response.status === 401)
                    throw new Error('Unauthorized');
                else
                    throw new Error('Error getting data from service: ' + response.statusText);
            }//**END if-else
        });//**END return fetch()
    }//**END private sendData()
    
    
}//**END class HarvestAPI













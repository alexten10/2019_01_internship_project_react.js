import * as React from "react";
import { Button } from "office-ui-fabric-react/lib/Button";
import { Dropdown, IDropdownOption } from "office-ui-fabric-react/lib/Dropdown";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { DefaultButton, PrimaryButton } from "office-ui-fabric-react/lib/Button";
import { Checkbox } from "office-ui-fabric-react/lib/CheckBox";
import { HarvestAPI } from "../HarvestAPI";
import { Entry } from "../DTO/Entry";


export interface AddEntryPageProps 
{
    api: HarvestAPI;
}


export interface AddEntryPageState 
{
    projectList: DropdownOption[];
    taskList: DropdownOption[];
    projectId: number;
    taskId: number;
    hours: string;
    selectedNotes: string; //** typed notes in add-in's 'Notes' textarea
    outlookNotes: string; //** notes from Outlook value (Office.body)
    includeMeetingContent: boolean; //** checkbox status
    isSaving: boolean;
    
    errorMessage: string;
    successMsg: string;//** used when new entry successfully created
    errorMessageProject: string;//** used when no project is selected (dropdown 'Project')
    errorMessageTask: string;//** used when no task is selected (dropdown 'Task')
    
    entryId: number; //** harvest entry ID
    entryProjectName: string;
    entryTaskName: string;
    entryNotes: string;
    entryHours: string;
    entryDate: string;
    isEntryData: boolean;//if specific entry data is recieved
    
    entryProjectId: number;
    entryTaskId: number;
}



class DropdownOption implements IDropdownOption
{
    public key: number;
    public text: string;
    public subtitle: string;
}


export class AddEntryPage extends React.Component<AddEntryPageProps, AddEntryPageState>
{
    private response: any;

    constructor(props: AddEntryPageProps) 
    {
        super(props);

        this.state = {
            projectList: [],
            taskList: [],
            projectId: -1,
            taskId: -1,
            selectedNotes: "",
            outlookNotes: "",
            hours: "",
            includeMeetingContent: false,
            isSaving: false,
            errorMessage: null,
            successMsg: null, 
            errorMessageProject: null,
            errorMessageTask: null,
            entryId: null,
            entryProjectName: null,
            entryTaskName: null,
            entryNotes: null,
            entryHours: null,
            entryDate: null,
            isEntryData: false,
            
            entryProjectId: null,
            entryTaskId: null,
        }
    }


    public componentDidMount() 
    {
        //********  START GETTING TIME AND BODY FROM OUTLOOK AND            ***************//
        //********  INFO OF ALL AVAILABLE PROJECTS AND TASKS FROM HARVEST   ***************//
        let item = Office.context.mailbox.item;
        let item_note = item.body;//**access body, which holds note value (note text)

        //**if outlook task has start time and finish time and getasync() method works
        if (item.start.getAsync && item.end.getAsync)
        {
            //**get note value (note text) from Outlook//**
            item_note.getAsync(Office.CoercionType.Text, (bodyResult) =>//**.Text - return data as text, .Html - as HTML
            {
                let item_note_value = bodyResult.value.trim();
                
                
                //   ##############  START GETTING INFO ABOUT SPECIFIC ENTRY FROM HARVEST  ##################   //
                //**get entry id from the outlook notes
                let str_match1 = 'HARVEST_ENTRY_ID';
                let str_match1_length = str_match1.length;
                
                //** if str_match1 value is found in the outlook notes
                if (item_note_value.indexOf(str_match1) !== -1) {
                    let index_of_str_match1 = item_note_value.indexOf(str_match1) + str_match1_length;//**index of last character in str_match1
                    let str_match2 = 'HARVEST_ENTRY_ID FINISH';
                    let index_of_str_match2 = item_note_value.indexOf(str_match2);
                    let associated_entry_id = item_note_value.substring(index_of_str_match1, index_of_str_match2); //**get entry id from outlook notes
                    let entry_id = parseInt(associated_entry_id);
                    this.setState({entryId: entry_id});
                    console.log("ASSOCIATED ENTRY_ID" + associated_entry_id);
                    
                    this.props.api.GetEntry(entry_id).then((entry_data: any) => {
                        if(entry_data) {
                            //console.log("ENTRY_ID_RESPONSE: " + JSON.stringify(entry_data));
                            let connected_entry_project = entry_data.project.name;
                            let connected_entry_task = entry_data.task.name;
                            let connected_entry_hours = entry_data.hours;
                            let connected_entry_notes = entry_data.notes;
                            let connected_entry_date = entry_data.spent_date;
                            //console.log("connected_entry_data: " + connected_entry_task);
                            
                            this.setState({
                                entryProjectName: entry_data.project.name,
                                entryTaskName: entry_data.task.name,
                                entryHours: entry_data.hours,
                                hours: entry_data.hours,
                                entryNotes: entry_data.notes,
                                selectedNotes: entry_data.notes,
                                entryDate: entry_data.spent_date,
                                isEntryData: true,
                                
                                entryProjectId: entry_data.project.id,
                                entryTaskId: entry_data.task.id,
                            });
                        }//END if
                    })
                    .catch((error) => {//**if no entry_data recieved after the request
                        console.error('Error retrieving data by entry ID: ' + error);
                    });
                }//END if (item_note_value.indexOf(str_match1) !== -1)
                //   ##############  END GETTING INFO ABOUT SPECIFIC ENTRY FROM HARVEST  ##################   //
                
                
                //**if Outlook notes is not empty, save it to the 'this.state.outlookNotes'
                if(item_note_value !== '') {
                    this.setState({ outlookNotes: "\n" + item_note_value });
                }
            });//END item_note.getAsync()

            
            item.start.getAsync((startResult: any) =>//**get task start time from outlook
            {
                item.end.getAsync((endResult: any) =>//**get task finish time from outlook
                {
                    let date = startResult.value;
                    let hours = (endResult.value - startResult.value) / (60 * 60 * 1000); // Get number of hours spent for the task

                    //**from Outlook: if time spent for task is checkbox "All day" (= 24 hours), save it as 8 hours (as full work day)
                    if (hours==24) {
                        this.setState({ hours: '8' });
                    }
                    else {//**convert a number of hours into a string, keeping only 2 decimals and save
                    this.setState({ hours: hours.toFixed(2) });
                    }//**END if-else

                    //**send request to get all available projects and tasks data for the day from Harvest
                    this.props.api.GetAvailProjects(startResult.value).then((data) =>
                    {
                        if (data)//**if data is recieved
                        {
                            //console.log("DATA: " + data.projects);//**check how many projects we got from GetAvailProjects()
                            this.response = data; // 'this.response' is used by onProjectChange()
                            let list = data.projects.map((project) =>//**loop through the received data (list of projects) and define values for each project
                            {
                                return {
                                    key: project.id,
                                    text: project.name,
                                    subtitle: 'Client: ' + project.client
                                };
                            });
                            this.setState({ projectList: list });
                            //console.log("AAAAAAAA: " + this.state.projectList[0].text);//**check values
                            //**now in the browser drop downs "Project" and "Task" are available
                        }//**END if(data)//**
                    }).catch((error) =>//**if no data recieved after the request
                    {
                        console.error('Error retrieving daily: ' + error);
                    });
                });//**END getAsync(endResult: any)
            });//**getAsync(startResult: any)
        }
        else//**if getAsync() method doesnt work
        {
            let start = new Date(item.start).getTime();
            let end = new Date(item.end).getTime();
            let date = item.start;
            let hours = (end - start) / (60 * 60 * 1000); // Get number of hours
            this.setState({ hours: hours.toFixed(2) });

            this.props.api.GetAvailProjects(item.start).then((data) =>
            {
                if (data)
                {
                    this.response = data;
                    let list = data.projects.map((project) =>
                    {
                        return {
                            key: project.id,
                            text: project.name,
                            subtitle: 'Client: ' + project.client
                        };
                    });
                    this.setState({ projectList: list });
                }
            }).catch((error) =>
            {
                console.error('Error retrieving daily: ' + error);
            });
        }//**ENF if-else//**

    }//**END public componentDidMount()//**


    //**when the button "save" is clicked/**
    private onSaveClicked() 
    {
        let item: any = Office.context.mailbox.item;
        
        if (this.state.projectId == -1) {
            this.setState({errorMessageProject : "You must select a Project"});
            return;
        }
        
        if (this.state.taskId == -1) {
            this.setState({errorMessageTask : "You must select a Task"});
            return;
        }
        
        //** parse a string 'hours' and return a floating point number, then fix to 2 decimals with rounding (ex: 2.50)
        let hours: number = +parseFloat(this.state.hours).toFixed(2);
        
        //** if typed value of "Hours" is not a number after parsing, the value will be 0
        if (isNaN(hours)) {
            hours = 0;
        }

        //** now "hours" is a number, reset state.errorMessage and set true for state.isSaving
        this.setState({ isSaving: true, errorMessage: null });

        if (item.start.getAsync)
        {
            item.start.getAsync((startResult) =>//**get the date of the task from outlook
            {
                //console.log('DATE ' + startResult.value);//**ex.   Thu Jan 31 2019 00:00:00 GMT-0600 (Central Standard Time)
                //IMPORTANT!!!! Outlook uses Local Time Zone(-6 hours for Canada), while Harvest uses ?Greenwich
                //so if you create a new entry on Harvest and send to it the date from oulook, correct to ?Greenwich first
                //or send to Harvest only date(yyyy-mm-dd) without time
                //ex. if outlook task time is 2019-01-19 23:00 , harvest will save it on 2019-01-20 (next day)
                
                //**preparing date for the request to create a new entry
                let start_date = new Date(startResult.value);
                let year_of_new_entry = start_date.getFullYear(); //**get year ex. 2019
                let month_of_new_entry = ("0" + (start_date.getMonth() + 1)).slice(-2);
                let day_of_new_entry = ("0" + start_date.getDate()).slice(-2);
                
                let date_of_new_entry = year_of_new_entry + '-'
                                       +month_of_new_entry + '-'
                                       +day_of_new_entry;//**ex. 2019-01-31
                
                //**prepare data for the new entry, which will be sent to create a new Harvest entry
                let newEntry = new Entry();
                newEntry.project_id = this.state.projectId;
                newEntry.task_id = this.state.taskId;
                newEntry.spent_date = date_of_new_entry;
                newEntry.hours = hours;

                let notes = this.state.selectedNotes;
                //** if checkbox "Include Outlook notes to be Saved on Harvest?" checked
                //** combine typed notes from 'Notes' textarea with Outlook notes
                if (this.state.includeMeetingContent)
                {
                    notes += '\n\n Notes from outlook below:' + this.state.outlookNotes;
                }
                newEntry.notes = notes;
                
                //**send data about new entry to the method CreateEntry() and if success save that data into this.state to display on success msg
                this.props.api.CreateEntry(newEntry).then((data: any) =>//** if just 'then(data) - typescript cant get data from array object, ex. data.id. so use 'then(data: any)'
                {
                    console.log('Create_entry_RESPONSE: ' + JSON.stringify(data));
                    this.setState({ isSaving: false, successMsg: "Entry is Created! Please, press SAVE in OUTLOOK to connect the new entry with this outlook task" });
                    
                    this.setState({ entryId: data.id,
                                    entryProjectName: data.project.name,
                                    entryTaskName: data.task.name,
                                    entryNotes: data.notes,
                                    entryHours: data.hours
                                 });
                    //console.log("ENTRY_ID" + JSON.stringify(this.state));
                    
                    /*
                    //** find out the format of outlook body (HTML or text)
                    item.body.getTypeAsync(//**Gets a value that indicates whether the content is in HTML or text format.
                        function (result: any) {
                            console.log(result.value);//** HTML or text
                        }
                    );
                    */
                    
                    //**text with entry ID that will be sent to outlook body if create new entry is successfull
                    let text_into_body = '<p>DO NOT change the following connecting this task and Harvest number: <br />HARVEST_ENTRY_ID' + this.state.entryId + 'HARVEST_ENTRY_ID FINISH</p><hr /><br />NOTES:';
                    //console.log("TEXT TO INSERT: " + text_into_body);
                    
                    //** insert into the outlook body a message with created entry id
                    item.body.prependAsync(
                        text_into_body,
                        { coercionType: Office.CoercionType.Html }
                    );
                }).catch((error) =>
                {
                    console.error(error);
                    this.setState({ isSaving: false, errorMessage: error });
                });//END catch()
            });//END item.start.getAsync()
        }//END if (item.start.getAsync)
        else
        {
            let newEntry = new Entry();
            newEntry.project_id = this.state.projectId;
            newEntry.task_id = this.state.taskId;
            newEntry.spent_date = item.start;//*****newEntry.spent_date = date_of_new_entry;
            newEntry.hours = hours;

            let notes = this.state.selectedNotes;
            if (this.state.includeMeetingContent)
            {
                notes +=  '\n\n Notes from outlook below:' + this.state.outlookNotes;
            }
            newEntry.notes = notes;
            
            this.props.api.CreateEntry(newEntry).then((data: any) =>
            {
                this.setState({ isSaving: false,  successMsg: "Entry is Created! Please, reload the Add-In." });
                this.setState({ entryId: data.id,
                                entryProjectName: data.project.name,
                                entryTaskName: data.task.name,
                                entryNotes: data.notes,
                                entryHours: data.hours
                             });
            }).catch((error) =>
            {
                console.error(error);
                this.setState({ isSaving: false, errorMessage: error });
            });
        }//END else
    }//**END private onSaveClicked()


    //**when the button "edit" is clicked/**
    private onEditClicked() 
    {
        if (this.state.projectId == -1) {
            this.setState({errorMessageProject : "You must select a Project"});
            return;
        }
        
        if (this.state.taskId == -1) {
            this.setState({errorMessageTask : "You must select a Task"});
            return;
        }
        
        //** parse a string 'hours' and return a floating point number, then fix to 2 decimals with rounding
        let hours: number = +parseFloat(this.state.hours).toFixed(2);
        
        //** if typed value of "Hours" is not a number, the value will be 0
        if (isNaN(hours)) {
            hours = 0;
        }

        //**prepare data for the new entry
        let updatedEntry = new Entry();
        updatedEntry.project_id = this.state.projectId;
        updatedEntry.task_id = this.state.taskId;
        updatedEntry.spent_date = this.state.entryDate;
        updatedEntry.hours = hours;
        updatedEntry.notes = this.state.selectedNotes;
        //console.log(JSON.stringify(updatedEntry));
        
        this.props.api.UpdateEntry(updatedEntry, this.state.entryId).then((data: any) =>//** if just 'then(data) - typescript cant get data from array object, ex. data.id. so use 'then(data: any)'
        {
            console.log('Create_entry_RESPONSE: ' + JSON.stringify(data));
            this.setState({ successMsg: "Entry is Updated!" });
            
            this.setState({ entryId: data.id,
                            entryProjectName: data.project.name,
                            entryTaskName: data.task.name,
                            entryHours: data.hours,
                            hours: data.hours,
                            entryNotes: data.notes,
                            selectedNotes: data.notes,
                            entryDate: data.spent_date,
                            isEntryData: true
                         });
            //console.log("ENTRY_ID" + JSON.stringify(this.state));
        })

    }//**END private onEditClicked()


    public render()
    {
        let disableAllFields = this.state.isSaving;
        let disableTaskDropdown = this.state.projectId === -1;

        const mainDivStyling = {
            paddingLeft: '10px',
            paddingRight: '10px',
            paddingBottom: '50px'
        }

        //** red color font for displaying error messages
        const errorColor = {
            color: 'red',
            fontSize: '1.5em'
        };

        //** green color font for displaying success message when an entry is created
        const successColor = {
            color: 'green',
            fontSize: '1.5em'
        };

        const redColor = {
            color: 'red'
        };

        let isSuccess = this.state.successMsg;
        let isEntryData = this.state.isEntryData;

        return (
            <div className="content-container" style={mainDivStyling}>
                <div className="content">

                    <span style={successColor}>{this.state.successMsg}</span>


                    {/*if specific entry data exists, display info about that entry and display form for editing that info*/}
                    {isEntryData==true ? (<div>
                        <h2 style={redColor}>This outlook task has been already synchronized with Harvest entry</h2>
                        <h3><u>Harvest entry summary:</u></h3>
                        <strong>Date of task: </strong>{this.state.entryDate}<br />
                        <strong>Project: </strong>{this.state.entryProjectName}<br />
                        <strong>Task: </strong>{this.state.entryTaskName}<br />
                        <strong>Hours: </strong>{this.state.entryHours}<br />
                        <strong>Notes: </strong>{this.state.entryNotes}
                        <hr />
                        <hr />
                        <h2>Change this entry info:</h2>
                        
                        <Dropdown
                            label="Project"
                            placeholder="select a project"
                            options={this.state.projectList}
                            onChanged={this.onProjectChanged.bind(this)}
                            //defaultSelectedKey={this.state.entryProjectId}
                            errorMessage={this.state.errorMessageProject}
                            onRenderOption={this.onRenderOption.bind(this)}
                            disabled={disableAllFields}
                        />

                        <br />
                        <Dropdown
                            label="Task"
                            placeholder="select a task"
                            options={this.state.taskList}
                            onChanged={this.onTaskChanged.bind(this)}
                            //defaultSelectedKey={this.state.entryTaskId}
                            errorMessage={this.state.errorMessageTask}
                            onRenderOption={this.onRenderOption.bind(this)}
                            disabled={disableAllFields || disableTaskDropdown}
                        />

                        <br />
                        <TextField
                            label="Hours"
                            value={this.state.hours.toString()}
                            onChanged={(value: string) => this.setState({ hours: value })}
                            onGetErrorMessage={this.onHoursErrorMessage.bind(this)}
                            disabled={disableAllFields}
                        />

                        <br />
                        <TextField
                            label="Notes"
                            value={this.state.selectedNotes}
                            onChanged={(value: string) => this.setState({ selectedNotes: value })}
                            placeholder="Put your notes here (optional)"
                            multiline
                            autoAdjustHeight
                            disabled={disableAllFields}
                        />
                        
                        <span style={successColor}>{this.state.successMsg}</span>
                        
                        <br />
                        <PrimaryButton
                            onClick={this.onEditClicked.bind(this)}
                            disabled={disableAllFields}>
                            Edit
                        </PrimaryButton>
                        
                    </div>) : (<div>

                                    {/* if no info about specific entry data exists,
                                        and no new entry created,
                                        display default form for a new entry creation*/}
                                    {/*JSX syntax for ternary conditional: if you want to return multiple elements 
                                       then wrap all the html code in a div or in any other wrapper component*/}
                                    {isSuccess==null ? (<div id='form'>

                                        <Dropdown
                                            label="Project"
                                            placeholder="select a project"
                                            options={this.state.projectList}
                                            onChanged={this.onProjectChanged.bind(this)}
                                            errorMessage={this.state.errorMessageProject}
                                            onRenderOption={this.onRenderOption.bind(this)}
                                            disabled={disableAllFields}
                                        />

                                        <br />
                                        <Dropdown
                                            label="Task"
                                            placeholder="select a task"
                                            options={this.state.taskList}
                                            onChanged={this.onTaskChanged.bind(this)}
                                            errorMessage={this.state.errorMessageTask}
                                            onRenderOption={this.onRenderOption.bind(this)}
                                            disabled={disableAllFields || disableTaskDropdown}
                                        />

                                        <br />
                                        <TextField
                                            label="Hours"
                                            value={this.state.hours.toString()}
                                            onChanged={(value: string) => this.setState({ hours: value })}
                                            onGetErrorMessage={this.onHoursErrorMessage.bind(this)}
                                            disabled={disableAllFields}
                                        />

                                        <br />
                                        <TextField
                                            label="Notes"
                                            value={this.state.selectedNotes}
                                            onChanged={(value: string) => this.setState({ selectedNotes: value })}
                                            placeholder="Put your notes here (optional)"
                                            multiline
                                            autoAdjustHeight
                                            disabled={disableAllFields}
                                        />

                                        <br />
                                        <div><strong>Outlook notes: </strong><br />{this.state.outlookNotes}</div>

                                        <br />
                                        <Checkbox
                                            label="Include Outlook notes to be Saved on Harvest?"
                                            checked={this.state.includeMeetingContent}
                                            onChange={(ev, checked: boolean) => { 
                                                if (this.state.includeMeetingContent == false) { //** if checked, set true
                                                    this.setState({ includeMeetingContent: true }); 
                                                }
                                                else { //** if unchecked, set false
                                                    this.setState({ includeMeetingContent: false });
                                                }
                                            } }
                                            disabled={disableAllFields}
                                        />

                                        <span className="error-message" style={errorColor} >{this.state.errorMessage}</span>

                                        <br />
                                        <PrimaryButton
                                            onClick={this.onSaveClicked.bind(this)}
                                            disabled={disableAllFields}>
                                            Create
                                        </PrimaryButton>


                                    </div>) : (<div>{/* if a new entry created, display new entry summary*/}
                                                    <br />
                                                    <h2><strong>Entry summary:</strong></h2><br />
                                                    <strong>Entry ID:</strong> {this.state.entryId}<br />
                                                    <strong>Project:</strong> {this.state.entryProjectName}<br />
                                                    <strong>Task:</strong> {this.state.entryTaskName}<br />
                                                    <strong>Hours:</strong> {this.state.entryHours}<br />
                                                    <strong>Notes:</strong> {this.state.entryNotes}
                                               </div>
                                    )}{/*END isSuccess==null*/}

                    </div>)}{/*END if specific entry data exists, display info about that entry and display form for editing that info*/}

                </div>{/*END .content*/}
            </div>//END .content-container
        );//END return()
    }//END render()


    //** starts when a Project is chosen in the add-in dropdown list 
    //** uses project.id to get its tasks and put them into an array this.state.taskList 
    //** returns task.id - as key, task.name - as text, and task.billable - as subtitle 
    private onProjectChanged(value: DropdownOption)
    {
        this.setState({errorMessageProject: null});//** erase error message for 'Project' input
        
        //** loop through the array of projects
        let project = this.response.projects.filter((p) =>
        {
            return p.id === value.key;
        });
        //console.log("PEOJECT_VALUE: " + project.title);

        if (project && project.length > 0)
        {
            project = project[0];
            let tasks = project.tasks.map((t) => {
                return {
                    key: t.id,
                    text: t.name,
                    subtitle: t.billable ? 'Type: Billable ' : 'Type: Non-Billable '
                };
            }).sort((a, b) => {
                var o1 = a.subtitle.toLowerCase();
                var o2 = b.subtitle.toLowerCase();
                var p1 = a.text.toLowerCase();
                var p2 = b.text.toLowerCase();

                if (o1 < o2) return -1;
                if (o1 > o2) return 1;
                if (p1 < p2) return -1;
                if (p1 > p2) return 1;
                return 0;
            });

            this.setState({
                projectId: value.key,
                taskList: tasks
            });
        }//END if
    }//END onProjectChanged()


    //** when a task is chosen in the add-in, use task.id to set this.state.taskId
    private onTaskChanged(value: DropdownOption)
    {
        this.setState({
            taskId: value.key,
            errorMessageTask: null //**erase error message for 'Task' input
        });
    }


    //**check if entered value in HOURS field is a number
    private onHoursErrorMessage(value: string)
    {
        let num = +value;

        if (isNaN(num) === false)
        {
            return '';
        }
        else
        {
            return 'Value must be a number';
        }
    }


    //** fires function that creates <option> dropdown for Project and Task fields in the add-in 
    private onRenderOption(option: DropdownOption)
    {
        return (
            <div className="dropdown-twoline">
                <span className="ms-font-m title">{option.text}</span><br />
                <span className="ms-font-s subtitle">{option.subtitle}</span>
                
            </div>
        );
    }


}//END class AddEntryPage






